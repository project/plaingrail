Full documentation on the Zen theme can be found in Drupal's Handbook:
  http://drupal.org/node/193318

Excellent documentation on Drupal theming can be found in the Theme Guide:
  http://drupal.org/theme-guide

Other specific features are developed at gausarts dot com

You must download and enable Zen prior to this theme! It really depends on zen.


------------------------------------!!!!!!INSTALLATION!!!!!!-----------------------------------

  1. Download plainGrail from http://drupal.org/project/plaingrail

  2. Unpack the downloaded file, take the entire plainGrail folder and place it in your
     Drupal installation under one of the following locations:
       sites/all/themes
         making it available to the default Drupal site and to all Drupal sites
         in a multi-site configuration
       sites/default/themes
         making it available to only the default Drupal site
       sites/example.com/themes
         making it available to only the example.com site if there is a
         sites/example.com/settings.php configuration file
       
       it is better to be placed outside of zen folder for easier upgrade to each!

  3. Log in as an administrator on your Drupal site and go to Administer > Site
     building > Themes (admin/build/themes) and enable plainGrail.

  4. Configure at Administer > Site building > Themes(admin/build/themes/settings/plaingrail)
     There are several width alternatives to generate dynamically under Theme-specific settings:
    
     grail-140x300 || grail-160x220 || grail-170x170 || more will be added if there is enough need
     
     Apart from defaults #sidebar-left = 130px  and #sidebar-right = 230px
     

  5. If you need any override, rename the provided CSS file (grail-overrides-rename.css ), 
     so that it is never overriden in every upgrade:
     
     grail-overrides-rename.css 
     
          change to:
     
     grail-overrides.css
     

     Once you have satifiying widths to your design, you may delete grail-devwidth.css for good
     

  6. Activate it via admin/build/themes/settings/plaingrail or 
     permanently in your plaingrail.info, by removing semicolon before the line:
     
     ;stylesheets[all][]   = grail-overrides.css
     
           change to
     
     stylesheets[all][]   = grail-overrides.css


  IMPORTANT: In Drupal 6, the theme system caches template files and which theme
  functions should be called. What that means is if you add a new theme or
  preprocess fuction to your template.php file or add a new template (.tpl.php)
  file to your sub-theme, you will need to rebuild the "theme registry."
  See http://drupal.org/node/173880#theme-registry


------------------------------------!!!!!!BLOCK AND REGIONS!!!!!!-----------------------------------

    See admin/build/block for visual representation or page.tpl for detailed explanations

    Out of the box block supports:
    1. Search form, go to admin/build/themes/settings/plaingrail, and enable "Search box"



------------------------------------!!!!!!Modules and misc customizations!!!!!!----------------------

    See template.php for each usage:
    1. imagecache and user picture,  see plaingrail_user_picture
    2. transliteration,  see Tranliteration section
    3. user profile categories, see User profile category
    4. maintenance page,  see plaingrail_maintenance_page. Go to
       http://drupal.org/node/195435 if you find trouble
    5. feed icon, see plaingrail_feed_icon
    6. more_link, see plaingrail_more_link



------------------------------------!!!!!!LAYOUT CUSTOMIZATIONS!!!!!!-----------------------------------

    Most of the layout stuffs go to layout.css.
    All static and dynamic holy grail techs go to grail-devwidth.css.
    Alternatives to play around:
    Default 130x230 || Fixed 140x300 || Fixed 160x220 || Fixed 170x170
    
    1. Change sidebar widths via admin/build/themes/settings/plaingrail

    2. Remove holy grail borders via admin/build/themes/settings/plaingrail or via grail-devwidth.css

    Most decoration stuffs go to plaingrail.css.
    Each file are heavily commented for explanation
    
    Grid Control:
         Control your layout with the Grid toggle button provided during your dev.
         Switch between 24 and 16 columns of Grid,
         you need to change it in plaingrail.css (.showgrid #page-inner).
         It's the last line there.

    
    Feel free to modify any of plainGrail files to suit your need. Comments
    and explanations are preserved in each of the files and CSS files
    1. plaingrail.css:
       * all decorations, administration, tables, and mostly unchanged styles
    2. layout.css:
       * all layout stuffs to customize placed here to spot things easier
    3. ads.css:
       * advertisement references from "Ad Unit Guidelines" and "Universal Ad Package",
       * disabled by default. You need to enable it in plaingrail.info
    4. grail-devwidth:
       * grail-devwidth STYLES, useful for dev purposes only
       * Delete this file for good, or use grail-overrides.css once you have created your columns method
     

    Always visit admin/build/themes/select to clear theme registry,
       or activate registry cleaner on theme settings,
       or even press key F5, to clear your browser cache


---------------!!!!!!HOLY GRAIL - PADDING 30PX (15px each on left and right)!!!!!!-------------------

See http://matthewjamestaylor.com/blog/ultimate-3-column-blog-style-pixels.htm

Important things to customize width of the columns, sample listed here is taken from grail-170x170.css:

You may reduce padding to total 20px or 10px each side, have a dummy/wireframe first.
The fixed width is set to 960px on #page on layout.css. You can make it fluid by removing 960px
from #page, and set margin or padding instead to have white spaces on each side.

Only the properties and values mentioned here are candidates to customize, use it wisely :)
Please note the order of the appearance as well. Do the adjustment one at a time. 
Give the actual width first, then adjust the position and margin.


/*-----------------------------!!!!!!BOTH SIDEBARS ACTIVE!!!!!!---------------------------------*/

Most important things to adjust from grail-devwidth.css into your grail-overrides.css are listed here:


To remove borders, remove the same section from layout.css for good,
or better override values to 0 here,
or best dynamically via admin/build/themes/settings/plaingrail for smooth upgrades to layout.css

#main-inner, .farleft{border-right:1px solid #eaeaea;}
#main{border-top:1px solid #eaeaea;}


#main{
min-height:320px;        /* Make it is not too short, such as in 404*/
background-color:#fff;   /* #sidebar-right background color which is the base of the other columns below*/
}

#sidebar-left{
width:170px;             /* Width of #sidebar-left minus/without left and right padding */
left:185px;              /* Width of #sidebar-left plus padding on the left, adjust .farleft accordingly*/
}

#sidebar-right{
width:170px;        	/* Width of #sidebar-right minus left and right padding */
left:555px;             /* Do some bargain here to reach the correct measure to your chosen width*/
}

/*corresponds to #sidebar-right*/
#main-inner{
background:#fff;        /* #sidebar-left background colour */
margin-left:-200px; 	/* Width of #sidebar-right, including total padding */
}

/*corresponds to #sidebar-left*/
.farleft{
background:#fff;    	/* First column background color where content goes */
margin-left:-200px; 	/* Width of #sidebar-left + total padding, adjust #sidebar-left */
}


/*This should be the last thing to do after all above values are set*/
#content-inner{
margin:20px 15px 20px 415px;	/* Content column side padding:
                             Left padding = first column width + left sidebar/middle column left padding width
                             Right padding = last column width + left sidebar/middle column right padding width */
}



/*-----------------------------!!!!!!ONE SIDEBAR ACTIVE!!!!!!---------------------------------*/
body.one-sidebar #main-inner{
margin-left:-400px; 	/* Width of #sidebar-right plus padding each */
}

body.one-sidebar .farleft{
margin-left:0; 	       /* Collapse #sidebar-left container */
}

body.one-sidebar #content{
}

body.one-sidebar #content-inner{
margin:20px 10px 20px 410px;	/* #sidebar-left side padding:
                       Left padding = Content width + left sidebar left padding width
                       Right padding = #sidebar-right width + left sidebar right padding width */
}

body.one-sidebar #sidebar-left{
left:390px;         	/* Width of the left-hand side padding (#sidebar-left width plus left padding)*/
width:380px;        	/* Width of #sidebar-left minus left and right padding */
}

body.one-sidebar #sidebar-right{
left:390px;
width:380px;        	/* Width of #sidebar-right minus left and right padding */
}




/*-----------------------------!!!!!!NO SIDEBAR ACTIVE!!!!!!----------------------------------*/
Most are fine, so leaving these out is okay:)

.no-sidebars  #content,.no-sidebars .farleft,
.no-sidebars #main-inner, .no-sidebars #main{
}

.no-sidebars #content-inner {
}



/*-----------------------------!!!!!!I M A G E S!!!!!!-----------------------------------------*/
rss-transparent.png is provided if you want to change navbar background color
from black to others. Just rename them to rss.png like the defaults. Use pngfix.module
to help IE6, or else add background color by image editor.

If you have more graphics which can be combined, 
embed them in your sprites.png and adjust their background positions accordingly.
Currently it holds grid buttons and search background.

Sprite is good to reduce http-requests to your server, so page can be served more quickly.


/*-----------------------------!!!!!!ADS!!!!!!-----------------------------------------*/

It's more usable as references during devs. It's disabled by default.


/*-----------------------------!!!!!!UPGRADING!!!!!!-----------------------------------------*/

Always keep grail-overrides.css and plaingrail.info safe. 
Put other custom CSS in your grail-overrides.css



