<?php

/**
 * @file template.php
 * Contains theme override functions and preprocess functions for the theme.
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   For more information on preprocess functions and template suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440
 *   and http://drupal.org/node/190815#template-suggestions
 */

/*
 *
 * Dynamically toggles call to certain .css via admin/build/themes/settings/plaingrail.
 */
   if (theme_get_setting('plaingrail_ads')) {
  drupal_add_css(path_to_theme() . '/ads.css', 'theme', 'all');
  }
  if (theme_get_setting('plaingrail_overrides')) {
  drupal_add_css(path_to_theme() . '/grail-overrides.css', 'theme', 'all');
  }
  if (theme_get_setting('plaingrail_layout') !== 'plaingrail_default' ) {
  drupal_add_css(path_to_theme() . '/grail-devwidth.css', 'theme', 'all');
  }

/**
 * Implementation of HOOK_theme().
 */
function plaingrail_theme(&$existing, $type, $theme, $path) {
  $hooks = zen_theme($existing, $type, $theme, $path);
  // Add your theme hooks like this:
  /*
  $hooks['hook_name_here'] = array( // Details go here );
  */
  // @TODO: Needs detailed comments. Patches welcome!
  return $hooks;
}

/**
 * Override or insert variables into all templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered (name of the .tpl.php file.)
 */
/* -- Delete this line if you want to use this function
function plaingrail_preprocess(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
/* -- Delete this line if you want to use this function // */
function plaingrail_preprocess_page(&$vars, $hook) {

  // Classes for body element. Allows advanced theming based on context
  // (home page, node of certain type, etc.)
  $classes = split(' ', $vars['body_classes']);
  // Remove the mostly useless page-ARG0 class.
  if ($index = array_search(preg_replace('![^abcdefghijklmnopqrstuvwxyz0-9-_]+!s', '', 'page-'. drupal_strtolower(arg(0))), $classes)) {
    unset($classes[$index]);
  }
  //Remove holy grail border
  if (theme_get_setting('plaingrail_border')) {
    $classes[] = 'noborder';
  }
  if (theme_get_setting('plaingrail_left_bg')) {
    $classes[] = 'nobg';
  }

  //Handle sidebar width alternatives, generated dynamically in admin/build/themes/settings/plaingrail

  if (theme_get_setting('plaingrail_layout') == 'plaingrail_170x170') {
    $classes[] = 'grail-170x170';
  }
  elseif (theme_get_setting('plaingrail_layout') == 'plaingrail_160x220') {
    $classes[] = 'grail-160x220';
  }
  elseif (theme_get_setting('plaingrail_layout') == 'plaingrail_140x300') {
    $classes[] = 'grail-140x300';
  }
  else {
    $classes[] = ''; //do not output any class to get defaults and so allow your own grail-overrides.css take place
  }

  $vars['body_classes_array'] = $classes;
  $vars['body_classes'] = implode(' ', $classes); // Concatenate with spaces.
}

/**
 * Override or insert variables into the node templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/* -- Delete this line if you want to use this function */
function plaingrail_preprocess_node(&$vars, $hook) {
    //This will add quick admin links such as edit and Delete along node display
    //Original code from http://11heaven.com
    // If we are in teaser view and have administer nodes permission
  if (user_access('administer nodes')) {
    // get the human-readable name for the content type of the node
    //$content_type_name = node_get_types('name', $vars['node']);
    // making a back-up of the old node links...
    $links = $vars['node']->links;
    $nid = $vars['node']->nid;
    $links['quick-edit'] = array(
      'title' => 'Edit',
      'href' => 'node/' . $nid . '/edit',
      'query' => drupal_get_destination(),
    );
    // and then adding the quick delete link
    $links['quick-delete'] = array(
      'title' => 'Delete',
      'href' => 'node/' . $nid . '/delete',
      'query' => drupal_get_destination(),
    );
	$vars['links'] = theme('links', $links, array('class' => 'links inline'));
  }
}


/**
 * Override or insert variables into the comment templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function plaingrail_preprocess_comment(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function plaingrail_preprocess_block(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
  * Customize the output of comment submission
*/
function plaingrail_comment_submitted($comment) {
  
  return t('<strong>!username</strong> | !datetime - !daterel',
    array(
      '!username' => theme('username', $comment),
      '!datetime' => format_date($comment->timestamp, 'custom', 'F j, Y'),
      '!daterel' =>  format_interval(time() - $comment->timestamp),
    ));
}

/**
  * Customize the output of node submission
*/
function plaingrail_node_submitted($node) {

  return t('<strong>!username</strong> | !datetime - !daterel',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created, 'custom', 'F j, Y'),
      '!daterel' =>  format_interval(time() - $node->created),
    ));
}

/** 
  * Customize drupal rss image here and also the title text
  * You replace the image as you please, adjust the class accordingly
  * If you want to change the icon position, change it in your page.tpl.php
*/

function plaingrail_feed_icon($url, $title = NULL) {
  $title = t('Stay updated on the news');
  if ($image = theme('image', path_to_theme() . '/images/rss.png', t('Stay updated with the news'), $title)) {
    return '<a href="'. check_url($url) .'" class="feed-icon">'. $image .'</a>';
  }
}

function plaingrail_more_link($url, $title) {
  return '<div class="more-link">['. t('<a href="@link" title="@title">see all</a>', array('@link' => check_url($url), '@title' => $title)) .']</div>';
}

/**
  * Theme override for theme_menu_item()
  * Add menu ID and custom Name based on menu titles for block menus, great for icons
  * If necessary this may replace primary links with block primary menu for advanced theming
*/
function plaingrail_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
  $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
  if (!empty($extra_class)) {
    $class .= ' '. $extra_class;
  }
  if ($in_active_trail) {
    $class .= ' active-trail';
  }

  // Add unique identifier
  static $item_id = 0;
  $item_id += 1;
  $id .= 'mid-' . $item_id;
  // Add semi-unique class, using zen_id_safe, based on menu item name
  $class_added = zen_id_safe(str_replace(' ', '_', strip_tags($link)));

  return '<li class="'. $class . ' ' . $class_added . '" id="' . $id . '">'. $link . $menu ."</li>\n";
}
/**
  * If you have trouble to create a custom maintenance page, see http://drupal.org/node/195435
*/
function plaingrail_maintenance_page($content, $messages = TRUE, $partial = FALSE) {
require_once 'maintenance-page.tpl.php';
}
