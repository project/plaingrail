<?php

/**
 * @file maintenance-page.tpl.php
 *
 * You must customize your own design. It's just a container starter for you to continue.
 * If you have trouble to create a custom maintenance page, see http://drupal.org/node/195435
 * It strips out all unnecessary thing such stylesheets, scripts, blocks, etc.
 * Since all database connection is disconnected for anonymous during maintenance.
 * You can even have a simple .html file instead. Just make sure to call it from template.php
 *
*/
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>">
  <head>
<title>Maintenance</title>
<link rel="shortcut icon" href="<?php print base_path() . path_to_theme() ?>/favicon.ico" type="image/x-icon" />

<style type="text/css">
body {background:#000;margin:0; padding:0; text-align: center;color:#000}
#wrapper {position:absolute; left:50%; width:100%;margin-left:-50%; text-align:left;padding-top:280px;}
.logo{float:left;}
img{border:0}
#container{text-align:center;height:34px;background:#ccc;border-top:2px solid #777;border-bottom:2px solid #777}
#footer{text-transform:uppercase;font-size:.9em}
#center{margin:0 auto;text-align: center;width:800px;line-height:32px;}
</style>

</head>
<body>

<!-- Layout -->
    <div id="wrapper">

      <div id="container"><div id="center">
          <a class="logo" href="<?php print $site_name; ?>" title="<?php print $site_name; ?>">
<img src="<?php print base_path() . path_to_theme() ?>/logo.png" alt="<?php print $site_name; ?>" id="logo" />
</a>

<?php print $content ?>
        </div> <!-- /#center -->
  

      </div> <!-- /#container -->
      <span class="clear"></span>
    </div> <!-- /#wrapper -->
  <?php print $closure ?>

  </body>
</html>
